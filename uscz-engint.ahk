;uscz for AutoHotkey on english international keyboard

Capslock::Esc
!c::WinClose A
;!h::WinSet, Style, ^0xF00000, A
!+b::WinSet, Style, ^0x800000, A
!+c::WinSet, Style, ^0xC00000, A
!+h::WinSet, Style, ^0x100000, A
!+v::WinSet, Style, ^0x200000, A


;---AltGr---
<^>!`::Send •
<^>!1::Send █
<^>!2::Send ²
<^>!3::Send ³
<^>!4::Send ⁴
<^>!5::Send ½
<^>!6::Send 😉
<^>!7::Send 😃
<^>!8::Send 😎
<^>!9::Send ☹
<^>!0::Send ☺
<^>!¥::Send –
<^>!=::Send ≠

<^>!q::Send €
<^>!w::Send ě
<^>!e::Send é
<^>!r::Send ř
<^>!t::Send ť
<^>!y::Send ý
<^>!u::Send ú
<^>!i::Send í
<^>!o::Send ó
<^>!p::Send π
<^>![::Send ÷
<^>!]::Send ×

<^>!a::Send á
<^>!s::Send š
<^>!d::Send ď
<^>!f::Send →
<^>!g::Send ↓
<^>!h::Send ů
<^>!j::Send ¥
<^>!k::Send ❤
<^>!l::Send £
<^>!¶::Send “
<^>!'::Send ”

<^>!z::Send ž
<^>!x::Send ✗
<^>!c::Send č
<^>!v::Send ✓
<^>!b::Send ₿
<^>!n::Send ň
<^>!m::Send μ
<^>!,::Send „
<^>!.::Send …
<^>!¿::Send 😕

;---Shift-AltGr---
<^>!+`::Send °
<^>!+1::Send §
<^>!+2::Send √
<^>!+3::Send ∛
<^>!+4::Send ®
<^>!+5::Send ‰
<^>!+6::Send 😊
<^>!+7::Send 😁
<^>!+8::Send 😘
<^>!+9::Send 😮
<^>!+0::Send ツ
<^>!+¥::Send —
<^>!+=::Send ±

<^>!+q::Send ß
<^>!+w::Send Ě
<^>!+e::Send É
<^>!+r::Send Ř
<^>!+t::Send Ť
<^>!+y::Send Ý
<^>!+u::Send Ú
<^>!+i::Send Í
<^>!+o::Send Ó
<^>!+p::Send 😛
<^>!+{::Send 🤷
<^>!+}::Send 🤦

<^>!+a::Send Á
<^>!+s::Send Š
<^>!+d::Send Ď
<^>!+f::Send ←
<^>!+g::Send ↑
<^>!+h::Send Ů
<^>!+j::Send ↵
<^>!+k::Send 👌
<^>!+l::Send 🍺
<^>!+¶::Send ‘
<^>!+"::Send ’

<^>!+z::Send Ž
<^>!+x::Send ♯
<^>!+c::Send Č
<^>!+v::Send 👍
<^>!+b::Send ♭
<^>!+n::Send Ň
<^>!+m::Send 🐈
<^>!+,::Send ≤
<^>!+>::Send ≥
<^>!+¿::Send 😴
